package dto;

import exceptions.ExcepcionRandom;

public class ExcepcionRandomRun {

	private static int numeroGenerado;
	
	// M�todo que genera un n�mero aleatorio y comprueba si es par o impar
	public static void excepcionRandomRun() {
		
		int num;
		
		try {
			
			num = ExcepcionNumeroAleatorio.excepcionNumeroAleatorio(1, 10);
			
			if (num % 2 == 0) {
				
				numeroGenerado = num;
				throw new ExcepcionRandom(1);
				
			} else {
				
				numeroGenerado = num;
				throw new ExcepcionRandom(2);
				
			}
			
		} catch (ExcepcionRandom e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}
	
	// M�todo que devuelve el n�mero aleatorio generado para su uso en otra clase
	public static int excepcionNumeroGenerado() {
		
		return numeroGenerado;
		
	}
	
}
