package exceptions;

import dto.ExcepcionRandomRun;

public class ExcepcionRandom extends Exception {

	private int codigoExcepcion;
	
	public ExcepcionRandom(int codigoError) {
		
		super();
		this.codigoExcepcion = codigoError;
		
	}
	
	public ExcepcionRandom() {
		
		super();
		
	}
	
	// M�todo que contiene el mensaje que se mostrar� por pantalla dependiendo de la excepci�n llamada
	public String getMessage() {
		
		String mensaje = "";
		
		switch (codigoExcepcion) {
		case 1:
			
			mensaje = "Generando n�mero aleatorio... \nEl numero aleatorio generado es: " + ExcepcionRandomRun.excepcionNumeroGenerado() + "\nEs par";
			break;
			
		case 2:
			
			mensaje = "Generando n�mero aleatorio... \\nEl numero aleatorio generado es: " + ExcepcionRandomRun.excepcionNumeroGenerado() + "\nEs impar";
			break;
			
		}
		
		return mensaje;
		
	}
	
}
