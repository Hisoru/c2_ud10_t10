package views;

import java.util.Scanner;

import dto.ExcepcionCalculosOperaciones;
import exceptions.ExcepcionCalculos;

public class ExcepcionCalculosView {
	
	// M�todo que recibe el tipo de operaci�n que el usuario desea calcular
	public static int operacion() {
		
		Scanner sc = new Scanner(System.in);
		int operacion = sc.nextInt();
		
		return operacion;
		
	}
	
	// M�todo que pregunta los n�meros necesarios para las operaciones y muestra el resultado
	public static void input(int operacion) throws ExcepcionCalculos {
		
		Scanner sc = new Scanner(System.in);
		
		double primerNumero;
		double segundoNumero;
		double resultado = 0;
		
		switch (operacion) {
		case 1:
			
			System.out.println("Introduce un n�mero");
			primerNumero = sc.nextDouble();
			
			System.out.println("Introduce otro n�mero");
			segundoNumero = sc.nextDouble();
			
			try {
				
				if (primerNumero <= 0 || segundoNumero <= 0) {
					
					throw new ExcepcionCalculos(1);
					
				} else {
					
					resultado = ExcepcionCalculosOperaciones.suma(primerNumero, segundoNumero);
					
				}
				
			} catch (ExcepcionCalculos e) {
				
				System.out.println(e.getMessage());
				
			}
			
			break;
			
		case 2:
			
			System.out.println("Introduce un n�mero");
			primerNumero = sc.nextDouble();
			
			System.out.println("Introduce otro n�mero");
			segundoNumero = sc.nextDouble();
			
			try {
				
				if (primerNumero <= 0 || segundoNumero <= 0) {
					
					throw new ExcepcionCalculos(1);
					
				} else {
					
					resultado = ExcepcionCalculosOperaciones.resta(primerNumero, segundoNumero);
					
				}
				
			} catch (ExcepcionCalculos e) {
				
				System.out.println(e.getMessage());
				
			}
			
			break;
			
		case 3:
			
			System.out.println("Introduce un n�mero");
			primerNumero = sc.nextDouble();
			
			System.out.println("Introduce otro n�mero");
			segundoNumero = sc.nextDouble();
			
			try {
				
				if (primerNumero <= 0 || segundoNumero <= 0) {
					
					throw new ExcepcionCalculos(1);
					
				} else {
					
					resultado = ExcepcionCalculosOperaciones.multiplicacion(primerNumero, segundoNumero);
					
				}
				
			} catch (ExcepcionCalculos e) {
				
				System.out.println(e.getMessage());
				
			}
						
			break;
			
		case 4:
	
			System.out.println("Introduce un n�mero");
			primerNumero = sc.nextDouble();
			
			System.out.println("Introduce otro n�mero");
			segundoNumero = sc.nextDouble();
			
			try {
				
				if (primerNumero <= 0 || segundoNumero <= 0) {
					
					throw new ExcepcionCalculos(1);
					
				} else {
					
					resultado = ExcepcionCalculosOperaciones.potencia(primerNumero, segundoNumero);
					
				}
				
			} catch (ExcepcionCalculos e) {
				
				System.out.println(e.getMessage());
				
			}
			
			break;
	
		case 5:
	
			System.out.println("Introduce un n�mero");
			primerNumero = sc.nextDouble();
			
			try {
				
				if (primerNumero <= 0) {
					
					throw new ExcepcionCalculos(1);
					
				} else {
					
					resultado = ExcepcionCalculosOperaciones.raizCuadrada(primerNumero);
					
				}
				
			} catch (ExcepcionCalculos e) {
				
				System.out.println(e.getMessage());
				
			}
			
			break;
	
		case 6:
	
			System.out.println("Introduce un n�mero");
			primerNumero = sc.nextDouble();
			
			try {
				
				if (primerNumero <= 0) {
					
					throw new ExcepcionCalculos(1);
					
				} else {
					
					resultado = ExcepcionCalculosOperaciones.raizCubica(primerNumero);
					
				}
				
			} catch (ExcepcionCalculos e) {
				
				System.out.println(e.getMessage());
				
			}
			
			break;

		case 7:
			
			System.out.println("Introduce un n�mero");
			primerNumero = sc.nextDouble();
			
			System.out.println("Introduce otro n�mero");
			segundoNumero = sc.nextDouble();
			
			try {
				
				if (primerNumero <= 0 || segundoNumero <= 0) {
					
					throw new ExcepcionCalculos(1);
					
				} else {
					
					resultado = ExcepcionCalculosOperaciones.division(primerNumero, segundoNumero);
					
				}
				
			} catch (ExcepcionCalculos e) {
				
				System.out.println(e.getMessage());
				
			}
			
			break;
			
		default:
			
			System.out.println("Introduce un n�mero v�lido");
			break;
			
		}
		
		System.out.println("El resultado de la operaci�n es: " + resultado);
		
	}
	
}
