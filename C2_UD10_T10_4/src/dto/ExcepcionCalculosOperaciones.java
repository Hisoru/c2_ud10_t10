package dto;

// Clase que contiene un m�todo distinto por cada tipo de operaci�n
public class ExcepcionCalculosOperaciones {

	public static double suma(double primerNumero, double segundoNumero) {
		
		double resultado = primerNumero + segundoNumero;
		
		return resultado;
		
	}
	
	public static double resta(double primerNumero, double segundoNumero) {
		
		double resultado = primerNumero - segundoNumero;
		
		return resultado;
		
	}

	public static double multiplicacion(double primerNumero, double segundoNumero) {
	
	double resultado = primerNumero * segundoNumero;
	
	return resultado;
	
	}

	public static double potencia(double primerNumero, double segundoNumero) {
	
	double resultado = (double) Math.pow(primerNumero, segundoNumero);
	
	return resultado;
	
	}

	public static double raizCuadrada(double numero) {
	
	double resultado = (double) Math.sqrt(numero);
	
	return resultado;
	
	}
	
	public static double raizCubica(double numero) {
		
		double resultado = (double) Math.cbrt(numero);
		
		return resultado;
		
	}
	
	public static double division(double primerNumero, double segundoNumero) {
		
		double resultado = primerNumero / segundoNumero;
		
		return resultado;
		
	}
	
}
