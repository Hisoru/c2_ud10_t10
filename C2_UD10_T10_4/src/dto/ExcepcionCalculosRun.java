package dto;

import java.util.Scanner;

import exceptions.ExcepcionCalculos;
import views.ExcepcionCalculosView;

public class ExcepcionCalculosRun {
	
	// M�todo que muestra un men� y pide al usuario un tipo de operaci�n
	public static void excepcionCalculosRun() throws ExcepcionCalculos {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("C�lculos simples");
		System.out.println("1. Suma");
		System.out.println("2. Resta");
		System.out.println("3. Multiplicaci�n");
		System.out.println("4. Potencia");
		System.out.println("5. Ra�z cuadrada");
		System.out.println("6. Ra�z cubica");
		System.out.println("7. Divisi�n");
		
		System.out.println();
		
		System.out.print("Selecciona un tipo de operaci�n: ");
		int operacion = sc.nextInt();
		
		ExcepcionCalculosView.input(operacion);
		
	}
	
}
