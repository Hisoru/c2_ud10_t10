package exceptions;

public class ExcepcionCalculos extends Exception {

	private int codigoExcepcion;
	
	public ExcepcionCalculos(int codigoError) {
		
		super();
		this.codigoExcepcion = codigoError;
		
	}
	
	public ExcepcionCalculos() {
		
		super();
		
	}
	
	// M�todo que contiene el mensaje que se mostrar� por pantalla si la excepci�n con el c�digo '1' es llamada
	public String getMessage() {
		
		String mensaje = "";
		
		switch (codigoExcepcion) {
		case 1:
			
			mensaje = "Introduce un n�mero mayor de cero";
			break;
			
		}
		
		return mensaje;
		
	}
	
}
