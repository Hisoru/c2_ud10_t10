package dto;

public class Password {

	private static int longitud;
	private static String contrasena;
	
	// Constructor por defecto
	public Password() {
		
		this.longitud = 8;
		this.contrasena = "";
		
	}
	
	// Constructor con la longitud que nosotros le pasemos, generara una contrase�a aleatoria con esa longitud
	public Password(int longitud) {
		
		this.longitud = longitud;
		this.contrasena = generarPassword(longitud);
		
	}
	
	public int getLongitud() {
		
		return longitud;
		
	}

	public void setLongitud(int longitud) {
		
		this.longitud = longitud;
		
	}

	public String getContrasena() {
		
		return contrasena;
		
	}
	
	// M�todo que genera una contrase�a aleatoria con la longitud introducida
	public static String generarPassword(int longitud) {
		
		char[] array = new char[longitud];
		
		for (int i = 0; i < longitud; i++) {
			
			char numeroAleatorio = (char) (Math.random() * ((122 - 48) + 1) + 48);
			array[i] = numeroAleatorio;
			
		}
		
		String password = new String(array);
		
		return password;
		
	}
	
	// M�todo que devuelve un boolean indicando si la contrase�a es fuerte o no
	public static boolean esFuerte() {
		
		int mayusculas = 0;
		int minusculas = 0;
		int numeros = 0;
		
		for (int i = 0; i < longitud; i++) {
			
			if (contrasena.charAt(i) >= 97 && contrasena.charAt(i) <= 122) {
				
				minusculas++;
				
			} else if (contrasena.charAt(i) >= 65 && contrasena.charAt(i) <= 90) {
				
				mayusculas++;
				
			} else {
				
				numeros++;
				
			}
			
		}
		
		if (mayusculas > 2 && minusculas > 1 && numeros > 5) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
	}

	@Override
	public String toString() {
		
		return "Password [longitud=" + longitud + ", contrasena=" + contrasena + "]";
		
	}
	
}
