package exceptions;

import java.util.InputMismatchException;

import views.PasswordView;

public class PasswordException {

	// M�todo que controla si el valor introducido es v�lido o no
	public static int passwordException() {
		
		int num = 0;
		
		try {
			
			num = PasswordView.passwordException();
			
		} catch (InputMismatchException e) {
			
			System.out.println("No has introducido un n�mero v�lido");
			
		}
		
		return num;
		
	}
	
}
