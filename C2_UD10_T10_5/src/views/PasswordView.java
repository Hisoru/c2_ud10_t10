package views;

import java.util.Scanner;

import dto.Password;
import exceptions.PasswordException;

public class PasswordView {

	static Scanner sc = new Scanner(System.in);
	
	// M�todo que pregunta el tama�o del array y la longitud de la contrase�a y muestra tanto el valor como si es fuerte o no
	public static void passwordView() {
		
		System.out.println("Introduce un tama�o para el array");
		int longitudArray = PasswordException.passwordException();
		
		Password[] array = new Password[longitudArray];
		boolean[] arrayFuerte = new boolean[longitudArray];
		
		System.out.println("Introduce una longitud para la contrase�a");
		int longitud = PasswordException.passwordException();
		
		for (int i = 0; i < longitudArray; i++) {
			
			array[i] = new Password(longitud);
			arrayFuerte[i] = array[i].esFuerte();
			
			System.out.println(array[i]);
			System.out.println(arrayFuerte[i]);
			
		}
		
		
	}
	
	// M�todo que recoge el valor que el usuario introduce para poder lanzar la excepci�n
	public static int passwordException() {
		
		int num = sc.nextInt();
		return num;
		
	}
	
}
