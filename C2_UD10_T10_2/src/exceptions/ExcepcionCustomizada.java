package exceptions;

public class ExcepcionCustomizada extends Exception {

	private int codigoExcepcion;
	
	public ExcepcionCustomizada(int codigoError) {
		
		super();
		this.codigoExcepcion = codigoError;
		
	}
	
	public ExcepcionCustomizada() {
		
		super();
		
	}
	
	// M�todo que contiene el mensaje que se mostrar� por pantalla si la excepci�n con el c�digo '1' es llamada
	public String getMessage() {
		
		String mensaje = "";
		
		switch (codigoExcepcion) {
		case 1:
			
			mensaje = "Mensaje mostrado por pantalla \nExcepcion capturada con mensaje: Esto es un objeto Exception \nPrograma terminado";
			break;

		}
		
		return mensaje;
		
	}
	
}
