package dto;

import exceptions.ExcepcionCustomizada;

public class ExcepcionCustomizadaRun {

	// M�todo que lanza la excepci�n si el n�mero es m�s grande que cero
	public static void excepcionCustomizadaRun() {
		
		int num;
		
		try {
			
			num = 5;
			
			if (num >= 0) {
				
				throw new ExcepcionCustomizada(1);
				
			}
			
		} catch (ExcepcionCustomizada e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}
	
}
