package exceptions;

import javax.swing.JOptionPane;

import views.InputUsuario;

public class InputException {

	// Try catch que controla que el usuario no introduzca un n�mero incorrecto
	public static int inputException() {
		
		int inputNum = 0;
		
		try {
			
			inputNum = InputUsuario.inputUsuario();
			
		} catch (NumberFormatException a) {
			
			JOptionPane.showMessageDialog(null, "Has introducido un n�mero incorrecto");
			
		}
		
		return inputNum;
		
	}
	
}
