package dto;

public class NumeroAleatorio {

	public static int numeroAleatorio(int min, int max) {
		
		int num = min + (int) (Math.random() * ((max - min) + 1));
		
		return num;
		
	}
	
}
