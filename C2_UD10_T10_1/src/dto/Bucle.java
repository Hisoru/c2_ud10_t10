package dto;

import javax.swing.JOptionPane;

import exceptions.InputException;
import views.InputUsuario;

public class Bucle {

	int inputNum = InputException.inputException();
	int numeroAleatorio = NumeroAleatorio.numeroAleatorio(1, 500);
	
	// M�todo que muestra al usuario si el n�mero es mayor o menor o si lo ha adivinado
	public static boolean numeroAdivinado(int inputNum, int numeroAleatorio) {
		
		if (inputNum > numeroAleatorio) {
			
			JOptionPane.showMessageDialog(null, "El n�mero que has introducido es mayor");
			return false;
			
		} else if (inputNum < numeroAleatorio)  {
		
			JOptionPane.showMessageDialog(null, "El n�mero que has introducido es menor");
			return false;
			
		} else {
			
			JOptionPane.showMessageDialog(null, "�Has adivinado el n�mero!");
			return true;
		
		}
		
	}
	
}
