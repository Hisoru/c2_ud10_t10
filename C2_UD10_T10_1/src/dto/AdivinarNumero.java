package dto;

import javax.swing.JOptionPane;

import exceptions.InputException;

public class AdivinarNumero {
	
	// M�todo que compara el n�mero introducido y el n�mero aleatorio y
	// muestra los intentos al usuario si ha conseguido acertar el n�mero
	public static void adivinarNumero() {
		
		int inputNum;
		int numeroAleatorio = NumeroAleatorio.numeroAleatorio(1, 500);
		int intentos = 1;
		
		boolean adivinado = false;
		
		do {
			
			inputNum = InputException.inputException();
			
			if (Bucle.numeroAdivinado(inputNum, numeroAleatorio)) {
				
				JOptionPane.showMessageDialog(null, "Intentos: " + intentos);
				adivinado = true;
				
			}
			
			intentos++;
			
		} while (!adivinado);
		
	}

}
