package views;

import javax.swing.JOptionPane;

import dto.Bucle;

public class InputUsuario {

	// M�todo que pregunta un n�mero al usuario
	public static int inputUsuario() {
		
		String inputNum = JOptionPane.showInputDialog("Introduce un n�mero");
		int inputNumInt = Integer.parseInt(inputNum);
		
		return inputNumInt;
		
	}
	
}
